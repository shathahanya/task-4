import React, { useState } from "react";
import { Container } from "react-bootstrap";
import useFetchJobs from "./Components/useFetchJobs";
import Job from "./Components/Job";
import JobsPagination from "./Components/JobsPagination";
import SearchForm from "./Components/SearchForm";
import logo from "./Assests/images/logo.PNG";
import style from "./Style/style.css";

function App() {
  const [params, setParams] = useState({});
  const [page, setPage] = useState(1);
  const { jobs, loading, error, hasNextPage } = useFetchJobs(params, page);

  //let jobsArr = Object.entries(jobs.results);
  //console.log(jobs)
  //console.log(params);
 

  //For Updating Search Params,takes an e that comes from our serach input
  function handleParamChange(e) {
    const param = e.target.name; //get param that we want to change
    const value = e.target.value; //get the value
    setPage(1); //go back to page 1 on search
    setParams(
      (prevParams) => {
        return { ...prevParams, [param]: value };
      } //make obj:take our all prevous params & set out new param to the new values
    );
  }

  return (
    <div>
      <nav>
        <img src={logo} />
      </nav>

      <SearchForm params={params} onParamChange={handleParamChange} />
      <Container className="my-4">
        <h1 className="mb-4">Recent Openings</h1>
        <div className="cards-container">
          {loading && <h1>Loading...</h1>}
          {error && <h1>Error. Try Refreshing.</h1>}
          {jobs &&
            jobs.results &&
            jobs.results.jobs.map((job, index) => {
              /*loop through each of our jobs and render the <Job> component = card*/
              return <Job key={index} job={job} />;
            })}
        </div>
        <JobsPagination
          page={page}
          setPage={setPage}
          hasNextPage={hasNextPage}
        />
      </Container>
    </div>
  );
}

export default App;
