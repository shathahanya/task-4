import React from 'react';
import { Form, Col } from 'react-bootstrap';

export default function SearchForm({ params, onParamChange }) {
  //console.log(params)
  
  return (
    <Form className="mb-4">
      <Form.Row >
        <Form.Group as={Col} >
         
          <Form.Control 
          onChange={onParamChange} 
          value={params.title}
          name="title" 
          type="text"
          placeholder="Job Title"
          />
        
        </Form.Group>
      </Form.Row>
    </Form>
  )
}
