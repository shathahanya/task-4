import React, { useState } from 'react'
import { Card,  Button, Collapse } from 'react-bootstrap'
import style from "../Style/style.css";
/*import ReactMarkdown from 'react-markdown' /*to transfer the markdown(discription link)
that we got from the API to react link to view how to apply discription*/

export default function Job({ job }) {
  //const [open, setOpen] = useState(false) //the view button to close and open the discription
  //console.log(job)
  return (
    
   // console.log(job.title)
   <Card className="mb-3 card">
      <Card.Body>
        <div className="d-flex justify-content-between">
          <div>
            <Card.Title className="card__title">
              {job.title}
            </Card.Title>
            <Card.Text variant="secondary" className="border-bottom">{job.location?.country}</Card.Text>
            {job.job_type.map((item) => (
              <Card.Text
                key={`${job.uuid}${item}`}
                variant="secondary"
                className="mr-2 border-bottom"
              >
                {item}
              </Card.Text>
            ))}
            
            {job.skills.map((item) => (
              <span
                key={`${job.uuid}${item}`}
                className="skillspan "
              >
                {item},
              </span>
            ))}
          </div>
        </div>
        <form action={job.url}>
          <input type="submit" value="View" className="btn-primary"/>
        </form>
      </Card.Body>
    </Card>
  )
}
