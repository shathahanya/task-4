import { useReducer, useEffect } from "react";
import axios from "axios";
import style from "../Style/style.css";

const ACTIONS = {
  MAKE_REQUEST: "make-request",
  GET_DATA: "get-data",
  ERROR: "error",
  UPDATE_HAS_NEXT_PAGE: "update-has-next-page",
};

const BASE_URL = "https://devapi-indexer.elevatustesting.xyz/api/v1/jobs";

function reducer(state, action) {
  switch (action.type) {
    case ACTIONS.MAKE_REQUEST:
      return { loading: true, jobs: [] };
    case ACTIONS.GET_DATA:
      return {
        ...state /*everything in our current state put it in a new state*/,
        loading: false,
        jobs: action.payload
          .jobs /*pass the jobs in the payload of our action */,
      };
    case ACTIONS.ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        jobs: [] /*clear out our jobs if we have an error*/,
      };
    case ACTIONS.UPDATE_HAS_NEXT_PAGE:
      return { ...state, hasNextPage: action.payload.hasNextPage };
    default:
      return state;
  }
}

export default function useFetchJobs(params, page) {
  const [state, dispatch] = useReducer(reducer, { jobs: [], loading: true });

  useEffect(() => {
    const cancelToken1 = axios.CancelToken.source(); //to stop so many Axios request at a time
    dispatch({ type: ACTIONS.MAKE_REQUEST });
    axios
      .get(BASE_URL, {
        headers: {
          "accept-company": "900a776e-a060-422e-a5e3-979ef669f16b",
        },
        cancelToken: cancelToken1.token,
        params: {
          language_profile_uuid: "ee5d991c-cdc6-4e83-b0b3-96f147208549",
          limit: 6,
          markdown: true /*holds the discription */,
          page: page,
          ...params /*Take our all current parameters and put them inside our parameters as well */,
        },
      })
      .then((res) => {
        dispatch({ type: ACTIONS.GET_DATA, payload: { jobs: res.data } });
        //console.log(res)
      })
      .catch((e) => {
        if (axios.isCancel(e)) return;
        dispatch({ type: ACTIONS.ERROR, payload: { error: e } });
      });

    const cancelToken2 = axios.CancelToken.source();
    axios
      .get(BASE_URL, {
        headers: {
          "accept-company": "900a776e-a060-422e-a5e3-979ef669f16b",
        },
        cancelToken: cancelToken2.token,
        params: {
          language_profile_uuid: "ee5d991c-cdc6-4e83-b0b3-96f147208549",
          limit: 6,
          markdown: true /*holds the discription */,
          page: page + 1,
          ...params /*Take our all current parameters and put them inside our parameters as well */,
        },
      })
      .then((res) => {
        dispatch({
          type: ACTIONS.UPDATE_HAS_NEXT_PAGE,
          payload: { hasNextPage: res.data.length !== 0 }, //if we have data, then there is a next page then hasNextPage=True
        });
        //console.log(res)
      })
      .catch((e) => {
        if (axios.isCancel(e)) return;
        dispatch({ type: ACTIONS.ERROR, payload: { error: e } });
      });

    return () => {
      //clean up
      cancelToken1.cancel();
      cancelToken2.cancel();
    };
  }, [params, page]);

  return state;
}
